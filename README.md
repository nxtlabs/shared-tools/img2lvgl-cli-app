## Instalacja wymaganych paczek

### Ubuntu

* `sudo apt-get install php7.0-cli`
* `sudo apt-get install php7.0-gd`

### Arch

* `pacman -S php-gd`

/etc/php/php.ini

```diff
- ;extension=gd
+ extension=gd
```

## Instalacja skryptu

* `sudo ./img2lvgl -c`

Należy również pamiętać, aby po sklonowaniu wywołać polecenie `git submodule update --init` które pobierze dołączony moduł.

## Pomoc

* `man img2lvgl`

## Przykład użycia

Zakładając, że w folderze `img` mamy grafikę w formacie `png` lub `jpeg` i chcemy aby wyniki znalazły się w tym samym folderze

* `img2lvgl img`
  
Wyniki mogą również trafić do innego folderu
  
* `img2lvgl src dist`

Skrypt nie nadpisuje istniejących plików. Aby wygenerować plik ponownie, usuń istniejący plik `.c` lub `.bin`.
